# README #

## Overview ##

For your sample project, you will make a web app with Firebase as the backend and either React or Angular2 for the frontend. The application should display restaurants nearby the user, and restaurants can be favorited by the user.

## Details ##

### Sign in screen ###

* Sign in with email/password using Firebase authentication
* Sign in with Facebook using Firebase authentication
* Sign up should include profile picture, name, email, password, confirm password

### Main Screen ###

* The main screen should contain two tabs where you can view the restaurant feed and the favorites feed
* Button which allows user to sign out and return to sign in screen

### Restaurant Feed ###

* Feed of restaurants 100km around you using the YELP API and the user's current location coordinates
* A picture of the restaurant
* The name of the restaurant
* A button which allows users to add/remove restaurants to their favorites which should be stored under the user's account using Firebase real time database
* Anything else you would like to add

### Favorites Tab ###

* A list of the current user's favorite restaurants

### BONUS ###

* Add pagination (loading restaurants as they scroll) to the restaurant feed
* Add detail restaurant feed items screen with rating system
* Anything else you want to add to this app. Make it your own!


## Next Steps ##

* If you don't fully grasp how the Firebase database and queries work this is a good video series called `The Firebase Database For SQL Developers` (https://www.youtube.com/playlist?list=PLl-K7zZEsYLlP-k-RKFa7RyNPa9_wCH2s)
* Create a Firebase project (https://firebase.google.com/)
* Start coding
* Make commits in the repository.